# TwicPics

## What is TwicPics?

[TwicPics](https://www.twicpics.com/?utm_source=github&utm_medium=organic&utm_campaign=components) is a **Responsive Media Service Solution** (SaaS) that enables **on-demand responsive image & video generation**.

With TwicPics, developers only deal with high-resolution versions of their media while end-users receive **optimized, perfectly sized, device-adapted** versions **delivered from a server close to them**.

TwicPics acts as an **proxy**. It retrieves your master file — from your own web server, cloud storage, or DAM — and generates a **device-adapted** version with **best-in-class compression**, delivered directly to the end-user from the **closest delivery point** available.

## Why this module?

This module allows a site builder to easily connect their `Drupal` site to the [TwicPics](https://www.twicpics.com/) solution which offers on-demand responsive image generation.
This service is combined with a smart and quiet JavaScript library, all powered by an intuitive, testable URL-based API.

Developers and contributors only deal with high-resolution versions of their media while end-users receive optimized, perfectly sized, device-adapted versions delivered from a server close to them.

## Features

The primary features include:

- Automatic processing of translations of all contribution visuals to the TwicPics API.
- Mapping Drupal styles to TwicPics effects (except desaturation).
  - Scale effect
  - Crop Effect
  - Scale and Crop effect
  - Resize Effect
  - Rotate Effect
- Enabling / Disabling `TwicPics` module.
- An administration interface to manage configuration of `TwicPics` domain and path.
- An administration interface to manage the maximum width and height of images for the entire site.
- When `TwicPics` module is enable, only the front part is affected by the translation with `TwicPics`. This allows local media viewing on the Back-office part.

## Installation

This module is installed like any other contributed module. For further
information, see [Installing Drupal Modules](https://drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration / standard usage scenario

- Install the module.
- Open `admin/config/services/twicpics`.
- Indicate the `TwicPics` domain used.
- Indicate the path defined in `TwicPics` for access to public files
- Eventually indicate the path defined in `TwicPics` for access to private files
- Indicate the version number of the `TwicPics` API used
- Specify a maximum image width (and if necessary a maximum height) for all contribution images on the site. This will limit possible contribution errors.

## Simplifying your media management

This module frees your team from the constraints of managing media on a site today.
It allows your team to focus on their contribution work and not have to deal with multi-size, multi-format, multi-resolution, and many other issues.

The main attraction of `TwicPics` is the ability to deliver perfectly sized media for all devices and connections, in real time.
But it is backed by a global CDN that allows for faster loading of media through a distributed cache.
All of this makes it possible to reduce the size of your files by up to 75%, without loss of quality, which makes it possible to integrate it into an eco-design approach.

## Uninstalling TwicPics

To uninstall the module, load the module uninstall page
(`/admin/modules/uninstall`) check "TwicPics" module and save.

## Maintainers / credits / contact

Written by [Sebastien Leduc](https://www.drupal.org/u/sleduc). 

Module's development and maintenance are currently sponsored by [Magnetic](https://www.linkedin.com/company/magnetic-coop).

The best way to contact the authors is to submit an issue, be it a support
request, a feature request or a bug report, in the [project issue queue](https://www.drupal.org/project/issues/twicpics).
