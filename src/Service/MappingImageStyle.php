<?php

namespace Drupal\twicpics\Service;

/**
 * The MappingImageStyle service. Does a bunch of mapping style.
 */
class MappingImageStyle {

  /**
   * Array to convert drupal style to twicpics style.
   *
   * @var array
   */
  private $arrayMappingEffect =
  [
    "crop_crop" => "crop",
    "image_convert" => "convert",
    "image_crop" => "crop",
    "image_desaturate" => "",
    "image_scale" => "contain",
    "image_scale_and_crop" => "cover",
    "image_resize" => "resize",
    "image_rotate" => "rotate",
  ];

  /**
   * Array to convert drupal focus to twicpics focus.
   *
   * @var array
   */
  private $arrayMappingFocus = [
    "left-top" => "top-left",
    "center-bottom" => "top",
    "right-top" => "top-right",
    "left-center" => "left",
    "center-center" => "center",
    "right-center" => "right",
    "left-bottom" => "bottom-left",
    "center-bottom" => "bottom",
    "right-bottom" => "bottom-right",
  ];

  /**
   * Array to convert output drupal image extension to twicpics image extension.
   *
   * @var array
   */
  private $arrayMappingOutput = [
    "jpeg" => "jpeg",
    "jpg" => "jpeg",
    "png" => "png",
    "webp" => "webp",
  ];

  /**
   * Injection of compute service.
   */
  public function __construct(Compute $compute) {
    $this->compute = $compute;
  }

  /**
   * Convert drupal effect to twicpics effect.
   *
   * @return string
   *   the builded url effect to pass to twicpics.
   */
  public function convert($effects) {

    $paramsUrl = "";

    foreach ($effects as $effect) {

      if (array_key_exists($effect->getSummary()['#effect']['id'], $this->arrayMappingEffect)) {

        $twicpicsEffect = $this->arrayMappingEffect[$effect->getSummary()['#effect']['id']];
        if ($twicpicsEffect != "") {
          $actualCover = "";
          $actualContain = "";
          $actualContainMax = "";
          $actualCrop = "";
          $actualFocus = "";
          $actualOutput = "";
          $actualResize = "";
          $actualTransform = "";

          $drupalData = array_key_exists('#data', $effect->getSummary()) ?
            $effect->getSummary()['#data'] :
            [];

          // Focus.
          if (array_key_exists('anchor', $drupalData) && array_key_exists($drupalData['anchor'], $this->arrayMappingFocus)) {
            $actualFocus = "/focus=" . $this->arrayMappingFocus[$drupalData['anchor']];
          }

          // Scale Effect.
          if ($twicpicsEffect == "contain") {
            $actualContain = $this->compute->computeSizeAttribute(
              "contain",
              $drupalData['width'],
              $drupalData['height'],
            );

            if (!$drupalData['upscale']) {
              $actualContainMax = $this->compute->computeSizeAttribute(
                "contain-max",
                $drupalData['width'],
                $drupalData['height'],
              );
            }
          }

          // Convert Effect.
          if ($twicpicsEffect == "convert") {
            $drupalMarkup = strtolower($effect->getSummary()['#markup']);
            if (array_key_exists($drupalMarkup, $this->arrayMappingOutput)) {
              $actualOutput = "/output=" . $this->arrayMappingOutput[$drupalMarkup];
            }
          }

          // Cover Effect.
          if ($twicpicsEffect == "cover") {
            $actualCrop = $this->compute->computeSizeAttribute(
              "cover",
              $drupalData['width'],
              $drupalData['height'],
            );
          }

          // Crop Effect.
          if ($twicpicsEffect == "crop" && !array_key_exists('crop_type', $drupalData)) {
            $actualCrop = $this->compute->computeSizeAttribute(
              "crop",
              $drupalData['width'],
              $drupalData['height'],
            );
          }

          // Resize Effect.
          if ($twicpicsEffect == "resize") {
            $actualResize = $this->compute->computeSizeAttribute(
              "resize",
              $drupalData['width'],
              $drupalData['height'],
            );
          }

          // Rotate Effect.
          if ($twicpicsEffect == "rotate") {
            $actualTransform = "/turn=" . (
              $drupalData['random'] ?
              rand(0, $drupalData['degrees']) :
              $drupalData['degrees']
            );
          }

          $paramsUrl .=
            $actualFocus .
            $actualCover .
            $actualContain .
            $actualContainMax .
            $actualCrop .
            $actualResize .
            $actualTransform .
            $actualOutput;
        }
      }
    }
    return $paramsUrl;
  }

}
