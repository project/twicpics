<?php

namespace Drupal\twicpics\Service;

/**
 * Class with methods to refactoring mapping attributes.
 */
class Compute {

  /**
   * Refactoring attribute size of mapping with Twicpics API constraints.
   */
  public function computeSizeAttribute($attr, $x, $y) {
    if (empty($x) & empty($y)) {
      return "";
    }
    else {
      $actualSize = (!empty($x)) ?
      (
        (!empty($y)) ?
          "{$x}x{$y}" :
          "{$x}x-"
      ) :
      "-x{$y}";
      return "/{$attr}={$actualSize}";
    }
  }

}
