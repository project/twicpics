<?php

namespace Drupal\twicpics\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Form for Twicpics settings.
 */
class TwicpicsForm extends ConfigFormBase {

  use StringTranslationTrait;

  /**
   * Regexp to validate input domaine.
   *
   * @var string
   */
  private $rValidDomain = '/^(?:https?:\/\/)?([^\/]+)\/*$/';

  /**
   * Regexp to validate input path.
   *
   * @var string
   */
  private $rValidPath = '/^\/*([^\/].+?)\/*$/';

  /**
   * Trim the value argument.
   */
  private function trimOrUndefined($value) {
    $trimmed = trim($value);
    return $trimmed !== '' ? $trimmed : NULL;
  }

  /**
   * Parse the domain by trim and regexp validation.
   */
  private function parseDomain($value) {
    $domain = $this->trimOrUndefined($value);
    if (!is_null($domain) && preg_match($this->rValidDomain, $domain)) {
      return preg_replace($this->rValidDomain, 'https://$1', $domain);
    }
    else {
      return "";
    }
  }

  /**
   * Parse the path by trim and regexp validation.
   */
  private function parsePath($value) {
    $path = $this->trimOrUndefined($value);
    if (!is_null($path) && preg_match($this->rValidPath, $path)) {
      return preg_replace($this->rValidPath, '/$1/', $path);
    }
    else {
      return "/";
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'twicpics_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'twicpics.admin_settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('twicpics.admin_settings');

    $form['twicpics_domain'] = [
      '#type' => 'textfield',
      '#name' => 'twicpics_domain',
      '#title' => $this->t('TwicPics Domain'),
      '#description' => $this->t('You can find your TwicPic domain in your TwicPics dashboard.'),
      '#size' => 50,
      '#default_value' => $config->get('twicpics_domain'),
      '#required' => TRUE,
      '#element_validate' => [
        function ($element, FormStateInterface $form_state) {
          $value = $form_state->getValue('twicpics_domain');
          $domain = $this->trimOrUndefined($value);
          if (is_null($domain)) {
            $form_state->setError($element, $this->t('Domain is mandatory.'));
          }
          elseif (!preg_match($this->rValidDomain, $value)) {
            $form_state->setError($element, $this->t('Invalid domain value.'));
          }
        },
      ],
    ];

    $form['twicpics_public_path'] = [
      '#type' => 'textfield',
      '#name' => 'twicpics_public_path',
      '#title' => $this->t('Path for public images'),
      '#size' => 50,
      '#description' => $this->t('This path is configured in the TwicPics back office for the specified domain.'),
      '#default_value' => $config->get('twicpics_public_path'),
      '#required' => FALSE,
    ];

    $form['twicpics_private_path'] = [
      '#type' => 'textfield',
      '#name' => 'twicpics_private_path',
      '#title' => $this->t('Path for private images'),
      '#size' => 50,
      '#description' => $this->t('This path is configured in the TwicPics back office for the specified domain.'),
      '#default_value' => $config->get('twicpics_private_path'),
      '#required' => FALSE,
    ];

    $form['twicpics_max_width'] = [
      '#type' => 'number',
      '#name' => 'twicpics_max_width',
      '#title' => $this->t('TwicPics Max Width (px)'),
      '#size' => 10,
      '#description' => $this->t('Maximum Width of optimized images. This prevent generating large variants.'),
      '#default_value' => $config->get('twicpics_max_width'),
      '#required' => FALSE,
    ];

    $form['twicpics_max_height'] = [
      '#type' => 'number',
      '#name' => 'twicpics_max_height',
      '#title' => $this->t('TwicPics Max Height (px)'),
      '#size' => 10,
      '#description' => $this->t('Maximum Height of optimized images. This prevent generating large variants.'),
      '#default_value' => $config->get('twicpics_max_height'),
      '#required' => FALSE,
    ];

    $form['twicpics_api_version'] = [
      '#type' => 'number',
      '#name' => 'twicpics_api_version',
      '#title' => $this->t('TwicPics API Version'),
      '#size' => 4,
      '#default_value' => NULL !== ($config->get('twicpics_api_version')) ? $config->get('twicpics_api_version') : 1,
      '#step' => 1,
      '#required' => TRUE,
    ];

    $form['actions']['#type'] = 'actions';

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
      '#button_type' => 'primary',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->config('twicpics.admin_settings')
      ->set('twicpics_domain', $this->parseDomain($form_state->getValue('twicpics_domain')))
      ->set('twicpics_public_path', $this->parsePath($form_state->getValue('twicpics_public_path')))
      ->set('twicpics_private_path', $this->parsePath($form_state->getValue('twicpics_private_path')))
      ->set('twicpics_api_version', $form_state->getValue('twicpics_api_version'))
      ->set('twicpics_max_width', $form_state->getValue('twicpics_max_width'))
      ->set('twicpics_max_height', $form_state->getValue('twicpics_max_height'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
